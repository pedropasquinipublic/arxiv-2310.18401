________________________________________________
<div align="center">
<h1> Alleviating the present tension between T2K and NOνA with neutrino New Physics at source </h1>    
</div>  

<div align="center">
<a href="https:/inspirehep.net/authors/1072726">
Adriano Cherchiglia</a><sup>1,2</sup>, 
<a href="https:/inspirehep.net/authors/1467863">
Pedro Pasquini</a><sup>3</sup>        

<a href="https:/inspirehep.net/authors/993806">
O.L.G. Peres</a><sup>1</sup>, 
F.F. Rodrigues<sup>1</sup>,
R.R. Rossi<sup>1,4</sup>,      
and
E.S. Souza<sup>1</sup>

(Creation date: 14 November 2023)

</div>

<sup>1</sup> Instituto de Física Gleb Wataghin - Universidade Estadual de Campinas (UNICAMP), 13083-859, Campinas SP, Brazil    
<sup>2</sup> Departamento de Física Teorica y del Cosmos, Universidad de Granada, Campus de Fuentenueva, E–18071 Granada, Spain    
<sup>3</sup> Department of Physics, University of Tokyo, Bunkyo-ku, Tokyo 113-0033, Japan    
<sup>4</sup> Instituto de Física Corpuscular, Universitat de Valencia, E-46980, Valencia, Spain 

________________________________________________


## Description
This project contains the Mathematica file used to obtain the ellipses for our work in [1].

## References

[1] A. Cherchiglia, Pedro Pasquini, O. L. G. Peres, F. F. Rodrigues, R. R. Rossi and E. S. Souza,
''<em>Alleviating the present tension between T2K and NO$\nu$A with neutrino New Physics at source</em>'',
[<a href="http://arxiv.org/abs/2310.18401">
arXiv:2310.18401</a> [hep-ph]].


